


***
# 爬虫学习
***
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/22.jpg)
## 第一章 入门
***
### 1.什么是爬虫？
##### 以下爬虫简介内容来源百度百科：
网络爬虫（又称为网页蜘蛛，网络机器人，在FOAF社区中间，更经常的称为网页追逐者），是一种按照一定的规则，自动地抓取万维网信息的程序或者脚本。另外一些不常使用的名字还有蚂蚁、自动索引、模拟程序或者蠕虫。

### 所需环境介绍：
 python
 vscode 或其他python IDE
 Anaconda python库环境管理软件

### 所需python库：
***
python库的安装方法：
`pip install 包名称` 就可以了
如果是anaconda环境，直接在软件中搜索点击安装就可以了
#### Requests
##### requests是一个很实用的Python HTTP客户端库。


##### 举个例子，以爬取西安房天下租房信息为例
网址：https://xian.zu.fang.com/

 ```python
 # -*- coding: utf-8 -*-
 import  requests
#请求头
headers = {
          'User-Agent': '123',
          }
#Get请求-并传递headers
data=requests.get("https://xian.zu.fang.com/",headers=headers)

print(data.text)

```
##### 输出结果为(部分节选)：
``` html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="zh-cn" />
    <meta property="og:type" content="image" />
    <meta property="og:image" content="https://imgwcs3.soufunimg.com/news/2020_09/17/d9530978-ffc2-44a7-84e8-6be1bdcce6b3.png" />
    <link rel="canonical" href="https://xian.zu.fang.com" />
    <meta name="applicable-device" content="pc" />
    <meta name="mobile-agent" content="format=html5;url=https://m.fang.com/zf/xian/" />
    <link rel="alternate" media="only screen and(max-width: 640px)" href="https://m.fang.com/zf/xian/" />

    <title>【西安租房网_西安租房信息|房屋出租】- 房天下</title>

    <meta name="keywords" content="西安租房，西安租房网，西安出租房源租房，租房信息" />

    <meta name="description" content="房天下西安租房网提供西安海量出租房源，实时更新，租房信息真实有效。西安租房子、找合租、个人房屋出租，就到房 
天下。" />

    <meta name="location" content="province=陕西;city=西安;coord=125.150148579,42.9863649464" />
    <link href="//static.soufunimg.com/esf/zu/zfonline/style2020/list/css/list2016test.css?v=2021112301" rel="stylesheet" />
</head>
```
requests的主要方法如下：
![requests的主要方法](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/22.png)


##### 获取请求头的方法
###### 打开谷歌浏览器
###### 1.打开你要爬取的网址，在空白处右键-->检查，然后点击 网络 全部
###### 2.刷新网页 在名称处选择对应网址 点击标头  划到最下面 `user-agent: `字段
```Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36 Edg/96.0.1054.34 ```


***
#### BeautifulSoup
Beautiful Soup 是用Python写的一个HTML/XML的解析器，它可以很好的处理不规范标记并生成剖析树(parse tree)。 它提供简单又常用的导航(navigating)，搜索以及修改剖析树的操作。它可以大大节省你的编程时间。
注意安装Beautiful Soup时根据自己的python版本选择，我安装的是bs4


##### 继续举例
```python
# -*- coding: utf-8 -*-
import  requests
import bs4
#请求头
headers = {
          'User-Agent': '13',
          }
#Get请求-并传递headers
data=requests.get("https://xian.zu.fang.com/",headers=headers)
soup=bs4.BeautifulSoup(data.text,"html.parser")
print(soup.prettify())

```
##### 输出结果为soup文档(部分节选)：
``` html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
  <meta content="zh-cn" http-equiv="Content-Language"/>
  <meta content="image" property="og:type"/>
  <meta content="https://imgwcs3.soufunimg.com/news/2020_09/17/d9530978-ffc2-44a7-84e8-6be1bdcce6b3.png" property="og:image"/>
  <link href="https://xian.zu.fang.com" rel="canonical"/>
  <meta content="pc" name="applicable-device"/>
  <meta content="format=html5;url=https://m.fang.com/zf/xian/" name="mobile-agent"/>
  <link href="https://m.fang.com/zf/xian/" media="only screen and(max-width: 640px)" rel="alternate"/>
  <title>
   【西安租房网_西安租房信息|房屋出租】- 房天下
  </title>
```

#### 常用的Beautiful Soup中方法有：
##### `select()`方法
##### `find_all( name , attrs , recursive , text , **kwargs )`方法
##### `find( name , attrs , recursive , text , **kwargs )`方法
#### 这里主要用下select方法，个人感觉很好用
    首先，`soup.select()`方法，返回类型是 `list`,通过某个属性查找，我们可以通过谷歌浏览器查找到需要的属性，具体方法为：
        1.在谷歌浏览器要爬取的页面选中需要爬取的数据，右键点击检查，
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/微信截图_20211125222328.png)

        2.在元素页面选中检查到的元素，右键点击复制，选择复制`selector`

![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/微信图片_20211125222637.png)

        3.将复制到的内容放到代码`select`方法中,但是需要把其中的css标签`dl:nth-child(1)`（意思就是选择d1的父元素下的第1个标签，如果为d1标签时输出结果）改为`dl:nth-of-type(1)`,（意思就是选择d1的父元素下的第1个d1标签，输出结果）


```python
# -*- coding: utf-8 -*-
import  requests
import bs4
#请求头
headers = {
          'User-Agent': '123',
          }
#Get请求-并传递headers
data=requests.get("https://xian.zu.fang.com/",headers=headers)
soup=bs4.BeautifulSoup(data.text,"html.parser")
selectdata = soup.select("#listBox > div.houseList > dl:nth-of-type(1) > dd > div.moreInfo > p > span")
for i in selectdata:
    print(i.get_text())
```

其中输出结果必须get_text()方法来获取其中的文字信息，否则会输出标签和文字。
#### 使用get_text()方法：
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/1111.png)
#### 不使用get_text()方法：
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/2222.png)

#### 此处`dl:nth-of-type(1)`的1，代表只获取第一个结果，若删除,改为`dl`，则为获取全部结果
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/1444.png)
#### 结果为：
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/111114.png)








## 未完待续，尽请期待！



---

欢迎关注我的公众号“**爱吃百香果的工程师**”，共同学习。

<center>
    <img src="https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/11.png" style="width: 100px;">
</center>


