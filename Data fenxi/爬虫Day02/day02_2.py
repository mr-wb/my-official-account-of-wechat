# -*- coding: utf-8 -*-
import  requests
import  bs4

def get_lastdata():
   headers = {
       'User-Agent': '213',
   }

   data=requests.get("https://xian.zu.fang.com/",headers=headers)
   soup=bs4.BeautifulSoup(data.text,"html.parser")
   # 获取标题
   title = soup.select("#listBox > div.houseList > dl > dd > p.title > a")
   # 获取价格
   price = soup.select("#listBox > div.houseList > dl > dd > div.moreInfo > p > span")
   # 获取具体的信息
   selectdata = soup.select("#listBox > div.houseList > dl > dd > p.font15.mt12.bold")   
   for title,price,selectdata in zip(title,price,selectdata):
       last_data={
           "标题":title.get_text().strip(),
           "具体信息":selectdata.get_text().strip(),
           "价格":price.get_text().strip()
       }

       print(last_data)

if __name__ == '__main__':
   get_lastdata()