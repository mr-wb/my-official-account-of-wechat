


***
# 爬虫学习Day02
***
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/22.jpg)
## 实战一下

### 继续昨天的例子，以爬取西安房天下租房信息为例
网址：https://xian.zu.fang.com/

 ```python
 # -*- coding: utf-8 -*-
import  requests
import bs4
#请求头
headers = {
          'User-Agent': '123',
          }
#Get请求-并传递headers
data=requests.get("https://xian.zu.fang.com/",headers=headers)
soup=bs4.BeautifulSoup(data.text,"html.parser")
selectdata = soup.select("#listBox > div.houseList > dl > dd > div.moreInfo > p > span")
for i in selectdata:
    print(i.get_text())
```
### 思考一下，这里我们只获取了租房的价格，其他的信息呢？
#### 标题
#### 价格
#### 具体内容


## 获取标题
### 继续昨天的方法，选中标题 ，右键检查，选择元素，右键复制，修改代码中select标签
```python
# -*- coding: utf-8 -*-
import  requests
import bs4
#请求头
headers = {
          'User-Agent': '123',
          }
#Get请求-并传递headers
data=requests.get("https://xian.zu.fang.com/",headers=headers)
soup=bs4.BeautifulSoup(data.text,"html.parser")
selectdata = soup.select("#rentid_D09_01_02 > a")
for i in selectdata:
    print(i.get_text())
```
### 输出结果为：
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/微信截图_20211125231750.png)
### 但是怎么获取所有的标题呢？
#### 这是我们发现使用select方法有点不合适了，仔细研究下网页代码，我们发现可以改为`#listBox > div.houseList > dl:nth-of-type(1) > dd > p.title > a`解决，同理，去掉`:nth-of-type(1)`,获取全部标题。
```python
# -*- coding: utf-8 -*-
import  requests
import bs4
#请求头
headers = {
          'User-Agent': '123',
          }
#Get请求-并传递headers
data=requests.get("https://xian.zu.fang.com/",headers=headers)
soup=bs4.BeautifulSoup(data.text,"html.parser")
selectdata = soup.select("#listBox > div.houseList > dl > dd > p.title > a")
for i in selectdata:
    print(i.get_text())

```
#### 输出结果为：
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/微信截图_20211125233218.png)

## 获取价钱 
### 昨天已经完成 我们得到的select方法为`#listBox > div.houseList > dl > dd > div.moreInfo > p > span`
## 获取详细信息
### 同理，我们得到的select方法为`#listBox > div.houseList > dl > dd > p.font15.mt12.bold`


## 整合代码
### 到此，把所有代码整合到一块，第一次实战完美结束：
```python
# -*- coding: utf-8 -*-
import  requests
import  bs4

def get_lastdata():
   headers = {
       'User-Agent': '213',
   }

   data=requests.get("https://xian.zu.fang.com/",headers=headers)
   soup=bs4.BeautifulSoup(data.text,"html.parser")
   # 获取标题
   title = soup.select("#listBox > div.houseList > dl > dd > p.title > a")
   # 获取价格
   price = soup.select("#listBox > div.houseList > dl > dd > div.moreInfo > p > span")
   # 获取具体的信息
   selectdata = soup.select("#listBox > div.houseList > dl > dd > p.font15.mt12.bold")   
   for title,price,selectdata in zip(title,price,selectdata):
       last_data={
           "标题":title.get_text().strip(),
           "具体信息":selectdata.get_text().strip(),
           "价格":price.get_text().strip()
       }

       print(last_data)

if __name__ == '__main__':
   get_lastdata()

```
### 最后输出的结果为：
![](https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/微信截图_20211126001404.png)

## 未完待续，尽请期待！




---

欢迎关注我的公众号“**爱吃百香果的工程师**”，共同学习。

<center>
    <img src="https://gitee.com/mr-wb/my-official-account-of-wechat/raw/master/doc/11.png" style="width: 100px;">
</center>


